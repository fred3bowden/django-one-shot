from django.urls import path
from todos.views import list_all, list_one, create_list, edit_list, delete_list, create_item, edit_item

urlpatterns = [
    path('', list_all, name="list_all"),
    path('<int:id>/', list_one, name="list_one"),
    path('create/', create_list, name="create_list"),
    path('<int:id>/edit/', edit_list, name="edit_list"),
    path('<int:id>/delete/', delete_list, name="delete_list"),
    path('items/create/', create_item, name="create_item"),
    path('items/<int:id>/edit/', edit_item, name="edit_item"),
]
