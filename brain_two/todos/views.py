from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

def list_all(request):
    list_objects = TodoList.objects.all()
    context = {
        "todo_objects": list_objects
    }

    return render(request, 'todos/list.html', context)

def list_one(request, id):
    list_object = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": list_object
    }

    return render(request, 'todos/item.html', context)

def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('list_one', id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }

    return render(request, 'todos/create.html', context)

def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect('list_one', id=id)
    else:
        form = TodoForm(instance=list)
    context = {
        "list_object": list,
        "form": form
    }

    return render(request, 'todos/edit.html', context)

def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect('list_one', id=item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "item_object": item,
        "form": form
    }

    return render(request, 'todos/edit_item.html', context)

def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect('list_all')

    return render(request, 'todos/delete.html')

def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('list_one', id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form
     }

    return render(request, 'todos/create_item.html', context)

def update_todo_item(request, id):
    todos = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todos)
        if form.is_valid():
            item = form.save()
            return redirect('list_one', id=item.list.id)
    else:
        form = TodoItemForm(instance=todos)
    context = {
        "form": form,
        "todos": todos
    }

    return render(request, 'todos/edit_item.html', context)
